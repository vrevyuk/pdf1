import Vue from 'vue'
import VueMeterial from 'vue-material'

import {MdApp} from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.css'
import 'vue-material/dist/theme/default.css'

Vue.use(MdApp)

export default VueMeterial
