import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/home'
import Files from '@/components/files'
import Viewer from '@/components/view_file'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/files',
      name: 'Files',
      component: Files
    },
    {
      path: '/file/:id',
      name: 'Viewer',
      component: Viewer
    }
  ]
})
