let files = [
  {id: 1, filename: '/static/pdf1.pdf'},
  {id: 2, filename: '/static/pdf2.pdf'},
  {id: 3, filename: '/static/pdf3.pdf'}
]

let notes = [
  {id: 1, pdfId: 1, comment: 'NOTE 1'},
  {id: 2, pdfId: 1, comment: 'NOTE 2'},
  {id: 3, pdfId: 1, comment: 'NOTE 3'},
  {id: 4, pdfId: 2, comment: 'NOTE 4'},
  {id: 5, pdfId: 2, comment: 'NOTE 5'},
  {id: 6, pdfId: 3, comment: 'NOTE 6'},
  {id: 7, pdfId: 3, comment: 'NOTE 7'},
  {id: 8, pdfId: 3, comment: 'NOTE 8'}
]

const getPdfFiles = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(files)
    }, 1000)
  })
}

const getNotesOfPdf = (pdfId) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(notes.filter(note => note.pdfId === pdfId))
    }, 1000)
  })
}

export {
  getPdfFiles,
  getNotesOfPdf
}
