export const setLoading = (state, loadingState) => (state.loading = loadingState)

export const setPdfs = (state, pdfs) => {
  state.pdfs = pdfs.map(pdf => ({...pdf, notes: []}))
}

export const setNotes = (state, {pdfId, notes}) => {
  state.pdfs = state.pdfs.map(pdf => {
    if (pdf.id === pdfId) {
      pdf.notes = notes
    }
    return pdf
  })
}
