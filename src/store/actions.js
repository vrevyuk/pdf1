import {getPdfFiles, getNotesOfPdf} from '../api'

export const fetchPdfs = async ({commit, state}) => {
  if (state.pdfs.length > 0) return
  try {
    commit('setLoading', true)
    const pdfs = await getPdfFiles()
    commit('setPdfs', pdfs)
    console.log(pdfs)
  } catch (e) {
    alert(e.message)
  } finally {
    commit('setLoading', false)
  }
}

export const fetchPdfNotes = async ({commit}, pdfId) => {
  try {
    commit('setLoading', true)
    const notes = await getNotesOfPdf(pdfId)
    commit('setNotes', {pdfId, notes})
    console.log(notes)
  } catch (e) {
    alert(e.message)
  } finally {
    commit('setLoading', false)
  }
}
