import Vue from 'vue'
import Vuex from 'vuex'

import * as getters from './getters'
import * as mutations from './mutations'
import * as actions from './actions'
import * as modules from './modules'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loading: false,
    pdfs: []
  },

  getters,
  actions,
  mutations,
  modules
})
